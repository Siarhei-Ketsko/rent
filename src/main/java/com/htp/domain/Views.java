package com.htp.domain;

public final class Views {

    public interface Create {}

    public interface Update {}

    public interface FindWithoutContracts {}

    public interface FindWithContracts {}

    public interface FindWithRepairContracts {}

    public interface FindWithoutRepairContracts {}

}
