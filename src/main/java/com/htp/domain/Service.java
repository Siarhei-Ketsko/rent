package com.htp.domain;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;

@Setter
@Getter
@RequiredArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(exclude = {
        "contractRepair"
})
@ToString(exclude = {
        "contractRepair"
})
@Entity
@Table(name = "m_service")
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class Service {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Long id;

    @Column(name = "service_name")
    private String serviceName;

    @Column(name = "service_address")
    private String serviceAddress;

    @JsonBackReference
    @JsonIgnoreProperties("service")
    @OneToOne(mappedBy = "service")
    private ContractRepair contractRepair;

    @Column(name = "is_deleted")
    @JsonIgnore
    private boolean deleted;

}
