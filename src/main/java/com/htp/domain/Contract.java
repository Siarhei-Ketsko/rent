package com.htp.domain;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Setter
@Getter
@RequiredArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(exclude = {
        "user", "tools"
})
@ToString(exclude = {
        "user", "tools"
})
@Entity
@Table(name = "m_contract")
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class Contract {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Long id;

    @Column(name = "contract_date")
    @Temporal(TemporalType.DATE)
    private Date contractDate;

    @Temporal(TemporalType.DATE)
    @Column(name = "issue_date")
    private Date issueDate;

    @Temporal(TemporalType.DATE)
    @Column(name = "return_date")
    private Date returnDate;

    @Column(name = "rent_price")
    private Double rentPrice;

    @Column(name = "contract_finished")
    private boolean finished;

    @JsonIgnoreProperties("contracts")
    @ManyToOne
    @JoinColumn(name = "users_id")
    private User user;

    @JsonIgnoreProperties("contracts")
    @ManyToMany(mappedBy = "contracts")
    private Set<Tool> tools = new HashSet<>();

}
