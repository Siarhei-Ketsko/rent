package com.htp.domain;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonView;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collections;
import java.util.Set;

@Setter
@Getter
@RequiredArgsConstructor
@AllArgsConstructor
@Entity
@EqualsAndHashCode(exclude = {
        "role", "contracts"
})
@ToString(exclude = {
        "role", "contracts"
})
@Table(name = "m_users")
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class User implements Serializable {

    @JsonView({Views.Update.class, Views.FindWithoutContracts.class})
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JsonView({Views.Update.class, Views.Create.class, Views.FindWithoutContracts.class})
    @Column(name = "username")
    private String username;

    @JsonView({Views.Update.class, Views.Create.class, Views.FindWithoutContracts.class})
    @Column(name = "surname")
    private String surname;

    @JsonView({Views.Update.class, Views.Create.class, Views.FindWithoutContracts.class})
    @Column(name = "patronymic")
    private String patronymic;

    @JsonView({Views.Update.class, Views.Create.class, Views.FindWithoutContracts.class})
    @Column(name = "phone")
    private String phone;

    @JsonView({Views.Update.class, Views.Create.class, Views.FindWithoutContracts.class})
    @Column(name = "series_passport")
    private String seriesPassport;

    @JsonView({Views.Update.class, Views.Create.class, Views.FindWithoutContracts.class})
    @Column(name = "number_passport")
    private Long numberPassport;

    @JsonView({Views.Update.class, Views.Create.class, Views.FindWithoutContracts.class})
    @Column(name = "address")
    private String address;

    @JsonView({Views.Create.class, Views.FindWithoutContracts.class})
    @Column(name = "login")
    private String login;

    @JsonView({Views.Update.class, Views.Create.class, Views.FindWithoutContracts.class})
    @JsonIgnore
    @Column(name = "password")
    private String password;

    @JsonView({Views.Update.class, Views.Create.class, Views.FindWithoutContracts.class})
    @Column(name = "email")
    private String email;


    @JsonView({Views.Update.class, Views.FindWithoutContracts.class})
    @Column(name = "verified")
    private boolean verified;

    @JsonIgnore
    @Column(name = "activation_code")
    private String  activationCode;

  //  @ApiModelProperty(hidden = true)
    @JsonIgnoreProperties("user")
    @JsonView({Views.Update.class, Views.Create.class, Views.FindWithContracts.class})
    @OneToMany(mappedBy = "user", fetch = FetchType.EAGER, orphanRemoval = true)
    private Set<Contract> contracts = Collections.emptySet();

    @JsonIgnoreProperties("user")
    @OneToOne(mappedBy = "user", cascade = {CascadeType.ALL}, fetch = FetchType.EAGER, orphanRemoval = true)
    @JsonIgnore
    private Role role;
}
