package com.htp.domain;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Setter
@Getter
@RequiredArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(exclude = {
        "tool", "service"
})
@ToString(exclude = {
        "tool", "service"
})
@Entity
@Table(name = "m_contract_repair")
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class ContractRepair implements Serializable {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Long id;

    @JsonIgnoreProperties("contractRepair")
    @OneToOne
    @JoinColumn(name = "service_id")
    private Service service;


    @JsonIgnoreProperties("contractRepair")
    @OneToOne
    @JoinColumn(name = "tools_id")
    private Tool tool;

    @Column(name = "total_cost_repair")
    private Double totalCostRepair;

}
