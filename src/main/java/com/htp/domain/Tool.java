package com.htp.domain;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;


import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.Set;

@Setter
@Getter
@RequiredArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(exclude = {
        "contracts" , "contractRepair"
})
@ToString(exclude = {
        "contracts" , "contractRepair"
})
@Entity
@Table(name = "m_tools")
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class Tool implements Serializable {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Long id;

    @Column(name = "brand")
    private String brand;

    @Column(name = "model")
    private String model;

    @Column(name = "personal_number")
    private String personalNumber;

    @Column(name = "price")
    private Double price;

    @Column(name = "availability")
    private boolean availability;

    @Column(name = "image")
    private String image;

    @JsonIgnore
    @Column(name = "reservation_date")
    private LocalDateTime reservationDate;

    @ApiModelProperty(hidden = true)
    @JsonBackReference
    @JsonIgnoreProperties("tool")
    @OneToOne(mappedBy = "tool")
    private ContractRepair contractRepair;


    @ApiModelProperty(hidden = true)
    @JsonBackReference
    @JsonIgnoreProperties("tools")
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "l_contract_tools",
            joinColumns = {@JoinColumn(name = "tools_id")},
            inverseJoinColumns = {@JoinColumn(name = "contract_id")}
    )
    private Set<Contract> contracts = Collections.emptySet();
}
