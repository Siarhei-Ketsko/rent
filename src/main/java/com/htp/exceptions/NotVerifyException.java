package com.htp.exceptions;

public class NotVerifyException extends RuntimeException {

    private static final String MESSAGE_ID_TEMPLATE = "%s with id = %s not verified";
    private static final String MESSAGE_TEMPLATE = "Dear %s %s %s you are not verified";

    public NotVerifyException() {
        super();
    }

    public NotVerifyException(String message) {
        super(message);
    }

    public NotVerifyException(String message, Throwable cause) {
        super(message, cause);
    }


    public NotVerifyException(Class<?> entityClass, Object name, Object surname) {
        super(String.format(MESSAGE_TEMPLATE, entityClass.getSimpleName(), name, surname));
    }
}
