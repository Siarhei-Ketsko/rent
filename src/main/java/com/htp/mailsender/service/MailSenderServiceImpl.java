package com.htp.mailsender.service;

import com.htp.config.MailConfiguration;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
public class MailSenderServiceImpl implements MailSenderService {

    private JavaMailSender mailSender;

    private MailConfiguration configuration;

    public MailSenderServiceImpl(JavaMailSender mailSender, MailConfiguration configuration) {
        this.mailSender = mailSender;
        this.configuration = configuration;
    }



    public void send(String emailTo, String subject, String message) {
        SimpleMailMessage mailMessage = new SimpleMailMessage();

        mailMessage.setFrom(configuration.getUsername());
        mailMessage.setTo(emailTo);
        mailMessage.setSubject(subject);
        mailMessage.setText(message);

        mailSender.send(mailMessage);
    }
}
