package com.htp.mailsender.util;

import com.htp.domain.Tool;
import com.htp.domain.User;

import java.time.format.DateTimeFormatter;

public class EmailMessageUtils {

    private EmailMessageUtils() {
    }

    public static String generateUserMessage(User user, Tool tool) {

        return "Dear " + user.getUsername() + " " + user.getSurname() + " You have reserved: " +
                tool.getModel() + " " + tool.getBrand() + " until " +
                tool.getReservationDate().format(DateTimeFormatter.ofPattern("HH:mm dd-MM-yyyy"));
    }

    public static String generateAdminMessage(User user, Tool tool) {

        return user.getUsername() + " " + user.getSurname() + " user id = " + user.getId() + " reserved " +
                tool.getBrand() + " " + tool.getModel() + " tool id = " + tool.getId() + " until " +
                tool.getReservationDate().format(DateTimeFormatter.ofPattern("HH:mm dd-MM-yyyy"));

    }

}
