package com.htp.config;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Configuration
@ConfigurationProperties("spring.mail")
public class MailConfiguration {

    private String host;

    private String username;

    private String password;

    private int port;

    private String protocol;

    private String adminMail;

    private Long timeReservation;

}
