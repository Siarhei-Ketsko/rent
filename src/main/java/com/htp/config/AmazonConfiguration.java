package com.htp.config;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Configuration
@ConfigurationProperties("amazon")
public class AmazonConfiguration {

    private String endpointUrl;

    private String accessKey;

    private String secretKey;

    private String region;

    private String bucketName;

}
