package com.htp.dao;

import com.htp.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;


public interface UserRepository extends CrudRepository<User, Long>, JpaRepository<User, Long>, PagingAndSortingRepository<User, Long> {


   Optional<User> findByLogin(String username);


   List<User> findAllByVerifiedIsTrue();

   User findByActivationCode(String code);

   User findByEmail(String email);


   @Query("select u from User u where " +
   ":surname is null or :surname='' or lower(u.surname) like lower(concat('%', :surname, '%')) " +
           "order by u.surname")
   List<User> findBySurname(@Param("surname") String surname);

}
