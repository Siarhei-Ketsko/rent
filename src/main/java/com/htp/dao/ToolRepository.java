package com.htp.dao;

import com.htp.domain.Tool;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;
import java.util.List;

public interface ToolRepository extends CrudRepository<Tool, Long>, JpaRepository<Tool, Long>, PagingAndSortingRepository<Tool, Long> {

    List<Tool> findAllByReservationDateBeforeAndAvailabilityTrue(LocalDateTime date);


    @Query("select t from Tool t where" +
            "(:brand is null or :brand='' or lower(t.brand) like lower(concat('%', :brand, '%')))" +
            "order by t.brand asc")
    List<Tool> findByBrand(@Param("brand") String brand);
}
