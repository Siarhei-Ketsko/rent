package com.htp.service;

import com.htp.dao.ContractRepository;
import com.htp.domain.Contract;
import com.htp.domain.Tool;
import com.htp.exceptions.EntityNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class ContractServiceImpl implements ContractService {

    private ContractRepository contractRepository;

    private ToolService toolService;

    public ContractServiceImpl(ContractRepository contractRepository, ToolService toolService) {
        this.contractRepository = contractRepository;
        this.toolService = toolService;
    }

    @Override
    public List<Contract> findAll() {


        return contractRepository.findAll();
    }

    @Override
    public Optional<Contract> findById(Long contractId) {
        return contractRepository.findById(contractId);
    }

    @Override
    public Contract findOne(Long contractId) {
        return contractRepository.findById(contractId).orElseThrow(() -> new EntityNotFoundException(Contract.class, contractId));
    }

    @Transactional
    @Override
    public Contract save(Contract contract) {
        contract.getTools().forEach(tool -> tool.setAvailability(false));
        return contractRepository.save(contract);
    }

    @Override
    public Contract update(Contract contract) {

        return contractRepository.save(contract);
    }

    @Override
    public void hardDelete(Long contractId) {
        contractRepository.deleteById(contractId);
    }


    @Transactional
    @Override
    public void deleteToolsFromContract(Long contractId, Long toolsId) {

        Contract contract = findOne(contractId);
        contract.getTools().removeIf(x -> x.getId().equals(toolsId));
        Tool tool = toolService.findOne(toolsId);
        tool.getContracts().removeIf(x -> x.getId().equals(contractId));
        toolService.save(tool);
        contract.setRentPrice(contract.getTools().stream().mapToDouble(Tool::getPrice).sum());
        contractRepository.save(contract);


    }

}
