package com.htp.service;


import com.htp.dao.ContractRepairRepository;
import com.htp.domain.ContractRepair;
import com.htp.exceptions.EntityNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
public class ContractRepairServiceImpl implements ContractRepairService {

    ContractRepairRepository contractRepairRepository;

    public ContractRepairServiceImpl(ContractRepairRepository contractRepairRepository) {
        this.contractRepairRepository = contractRepairRepository;
    }

    @Override
    public List<ContractRepair> findAll() {
        return contractRepairRepository.findAll();
    }

    @Override
    public Double allRentCostForTool(Long toolId) {
        return contractRepairRepository.allRentCostForTool(toolId);
    }

    @Override
    public Optional<ContractRepair> findById(Long contractRepairId) {
        return contractRepairRepository.findById(contractRepairId);
    }

    @Override
    public ContractRepair findOne(Long contractRepairId) {
        return contractRepairRepository.findById(contractRepairId).orElseThrow(()-> new EntityNotFoundException(ContractRepair.class, contractRepairId));
    }

    @Override
    public ContractRepair save(ContractRepair contractRepair) {
        return contractRepairRepository.save(contractRepair);
    }

    @Override
    public ContractRepair update(ContractRepair contractRepair) {
        return contractRepairRepository.save(contractRepair);
    }

    @Override
    public void hardDelete(Long contractRepairId) {
        contractRepairRepository.deleteById(contractRepairId);

    }
}
