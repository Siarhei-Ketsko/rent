package com.htp.service;

import com.htp.domain.Tool;

import java.security.Principal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface ToolService {

    List<Tool> findAll();

    List<Tool> findAllAvailable(LocalDateTime date);

    List<Tool> searchByBrand(String brand);

    Optional<Tool> findById(Long toolsId);

    Tool findOne(Long toolsId);

    Tool save(Tool tool);

    Tool update(Tool tool);

    void hardDelete(Long toolId);

    Tool reservationTool(Long toolId, Principal principal);

}
