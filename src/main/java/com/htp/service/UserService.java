package com.htp.service;

import com.htp.domain.User;

import java.util.List;
import java.util.Optional;

public interface UserService {

    List<User> findAll();

    List<User> searchBySurname(String surname);

    Optional<User> findById(Long userId);

    User findOne(Long userId);

    Optional<User> findByLogin(String username);

    User save(User user);

    User update(User user);

    void hardDelete(Long userId);

    List<User> findByIdAnAndVerifiedIsTrue();

}
