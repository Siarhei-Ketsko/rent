package com.htp.service;

import com.htp.dao.UserRepository;
import com.htp.domain.User;
import com.htp.exceptions.EntityNotFoundException;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@CacheConfig(cacheNames = {"usersAdmins"})
@Service
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public List<User> searchBySurname(String surname) {
        return userRepository.findBySurname(surname);
    }

    @Override
    public Optional<User> findById(Long userId) {
        return userRepository.findById(userId);
    }

    @Override
    public User findOne(Long userId) {
        return userRepository.findById(userId).orElseThrow(()-> new EntityNotFoundException(User.class, userId));
    }

    @Override
    public Optional<User> findByLogin(String username) {
        return userRepository.findByLogin(username);
    }

    @Override
    @Transactional
    public User save(User user) {
        return userRepository.save(user);
    }

    @Override
    @Transactional
    public User update(User user) {
        return userRepository.save(user);
    }

    @Override
    public void hardDelete(Long userId) {
        userRepository.deleteById(userId);
    }

    @Override
    @Cacheable
    public List<User> findByIdAnAndVerifiedIsTrue() {
        return userRepository.findAllByVerifiedIsTrue();
    }

}
