package com.htp.service;


import com.htp.domain.ContractRepair;

import java.util.List;
import java.util.Optional;

public interface ContractRepairService {

    List<ContractRepair> findAll();

    Double allRentCostForTool(Long toolId);

//    List<ContractRepair> search(String searchParam);

    Optional<ContractRepair> findById(Long contractRepairId);

    ContractRepair findOne(Long contractRepairId);

    ContractRepair save(ContractRepair contractRepair);

    ContractRepair update(ContractRepair contractRepair);

    void hardDelete(Long contractRepairId);


}
