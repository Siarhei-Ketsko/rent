package com.htp.service;

import com.htp.config.MailConfiguration;
import com.htp.dao.ToolRepository;
import com.htp.domain.Tool;
import com.htp.domain.User;
import com.htp.exceptions.EntityNotFoundException;
import com.htp.exceptions.NotVerifyException;
import com.htp.mailsender.service.MailSenderService;
import com.htp.mailsender.service.MailSenderServiceImpl;
import com.htp.security.util.PrincipalUtil;
import com.htp.mailsender.util.EmailMessageUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class ToolServiceImpl implements ToolService {

    ToolRepository toolRepository;

    UserService userService;

    MailSenderService mailSenderService;

    MailConfiguration configuration;

    public ToolServiceImpl(ToolRepository toolRepository, UserService userService, MailSenderServiceImpl mailSenderService, MailConfiguration configuration) {
        this.toolRepository = toolRepository;
        this.userService = userService;
        this.mailSenderService = mailSenderService;
        this.configuration = configuration;
    }

    @Override
    public List<Tool> findAll() {
        return toolRepository.findAll();
    }

    @Override
    public List<Tool> findAllAvailable(LocalDateTime date) {
        return toolRepository.findAllByReservationDateBeforeAndAvailabilityTrue(date);
    }

    @Override
    public List<Tool> searchByBrand(String brand) {
        return toolRepository.findByBrand(brand);
    }

    @Override
    public Optional<Tool> findById(Long toolsId) {
        return toolRepository.findById(toolsId);
    }

    @Override
    public Tool findOne(Long toolsId) {
        return toolRepository.findById(toolsId).orElseThrow(() -> new EntityNotFoundException(Tool.class, toolsId));
    }

    @Override
    public Tool save(Tool tool) {
        return toolRepository.save(tool);
    }

    @Override
    public Tool update(Tool tool) {
        return toolRepository.save(tool);
    }

    @Override
    public void hardDelete(Long toolId) {
        toolRepository.deleteById(toolId);
    }

    @Override
    public Tool reservationTool(Long toolId, Principal principal) {
        User user = userService.findByLogin(PrincipalUtil.getUsername(principal)).get();
        Tool tool = findOne(toolId);
        String adminMessage = EmailMessageUtils.generateAdminMessage(user, tool);
        String userMessage = EmailMessageUtils.generateUserMessage(user, tool);
        if (user.isVerified()) {
            tool.setReservationDate(LocalDateTime.now().plusHours(configuration.getTimeReservation()));
            mailSenderService.send(configuration.getAdminMail(), "Reservation", adminMessage);
            mailSenderService.send(user.getEmail(), "Reservation", userMessage);
            return save(tool);

        } else {
            throw new NotVerifyException(User.class, user.getUsername(), user.getSurname());

        }

    }


}
