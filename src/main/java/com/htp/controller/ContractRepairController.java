package com.htp.controller;

import com.htp.controller.request.ContractRepairCreateRequest;
import com.htp.controller.request.ContractRepairUpdateRequest;
import com.htp.domain.ContractRepair;
import com.htp.service.ContractRepairService;
import io.swagger.annotations.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.convert.ConversionService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/repair")
public class ContractRepairController {

    private ContractRepairService contractRepairService;

    private ConversionService conversionService;

    public ContractRepairController(ContractRepairService contractRepairService, ConversionService conversionService) {
        this.contractRepairService = contractRepairService;
        this.conversionService = conversionService;
    }

    @ApiOperation(value = "Finding all repair contracts")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Successful loading repair contracts"),
            @ApiResponse(code = 500, message = "Server error, something wrong")
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "X-Auth-Token", value = "token", required = true, dataType = "string", paramType = "header")
    })
    @GetMapping
    public ResponseEntity<List<ContractRepair>> findAll() {
        return new ResponseEntity<>(contractRepairService.findAll(), HttpStatus.OK);
    }

    @ApiOperation(value = "Finding repair contract by id")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Successful loading repair contract"),
            @ApiResponse(code = 500, message = "Server error, something wrong")
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "Repair contract database id", example = "1", required = true, dataType = "long", paramType = "path"),
            @ApiImplicitParam(name = "X-Auth-Token", value = "token", required = true, dataType = "string", paramType = "header")
    })
    @GetMapping("/{id}")
    public ContractRepair findById(@PathVariable("id") Long contractRepairId) {
        return contractRepairService.findOne(contractRepairId);
    }

    @GetMapping("/{id}/totalCostAllRepair")
    public Double totalCostAllRepair(@PathVariable("id") Long contractRepairId) {
        return contractRepairService.allRentCostForTool(contractRepairId);
    }

    @ApiOperation(value = "Create repair contract")
    @ApiResponses({
            @ApiResponse(code = 201, message = "Successful  creation repair contract"),
            @ApiResponse(code = 422, message = "Failed repair contract creation properties validation"),
            @ApiResponse(code = 500, message = "Server error, something wrong")
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "X-Auth-Token", value = "token", required = true, dataType = "string", paramType = "header")
    })
    @PostMapping
    public ResponseEntity<ContractRepair> create(@Valid @RequestBody ContractRepairCreateRequest contractRepairCreateRequest) {
        ContractRepair convertedContractRepair = conversionService.convert(contractRepairCreateRequest, ContractRepair.class);
        return new ResponseEntity<>(contractRepairService.save(convertedContractRepair), HttpStatus.OK);
    }

    @ApiOperation(value = "Update repair contract")
    @ApiResponses({
            @ApiResponse(code = 201, message = "Successful update repair contract"),
            @ApiResponse(code = 422, message = "Failed repair contract update properties validation"),
            @ApiResponse(code = 500, message = "Server error, something wrong")
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "contract database id", example = "1", required = true, dataType = "long", paramType = "path"),
            @ApiImplicitParam(name = "X-Auth-Token", value = "token", required = true, dataType = "string", paramType = "header")
    })
    @PutMapping(path = "/{id}")
    public ResponseEntity<ContractRepair> update(@PathVariable("id") Long contractRepairId, @Valid @RequestBody ContractRepairUpdateRequest contractRepairUpdateRequest) {
        contractRepairUpdateRequest.setContractRepairId(contractRepairId);
        ContractRepair convertedContractRepair = conversionService.convert(contractRepairUpdateRequest, ContractRepair.class);
        return new ResponseEntity<>(contractRepairService.save(convertedContractRepair), HttpStatus.OK);

    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<Long> delete(@PathVariable("id") Long contractRepairId) {
        contractRepairService.hardDelete(contractRepairId);
        return new ResponseEntity<>(contractRepairId, HttpStatus.OK);
    }

}
