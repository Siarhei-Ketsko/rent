package com.htp.controller;


import com.fasterxml.jackson.annotation.JsonView;
import com.htp.controller.request.UserCreateRequest;
import com.htp.controller.request.UserUpdateRequest;
import com.htp.domain.User;
import com.htp.domain.Views;
import com.htp.security.util.PrincipalUtil;
import com.htp.service.UserService;
import io.swagger.annotations.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.convert.ConversionService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/users")
public class UserController {

    private UserService userService;

    private ConversionService conversionService;

    public UserController(UserService userService, ConversionService conversionService) {
        this.userService = userService;
        this.conversionService = conversionService;
    }

    @ApiOperation("Find all users")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Successful loading user"),
            @ApiResponse(code = 500, message = "Server error, something wrong")

    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "X-Auth-Token", value = "token", required = true, dataType = "string", paramType = "header")
    })
    @GetMapping
    @JsonView(Views.FindWithoutContracts.class)
    public ResponseEntity<List<User>> findAll() {
        return new ResponseEntity<>(userService.findAll(), HttpStatus.OK);
    }

    @ApiOperation("Search user by id")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Successful loading user"),
            @ApiResponse(code = 500, message = "Server error, something wrong")
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "X-Auth-Token", value = "token", required = true, dataType = "string", paramType = "header"),
            @ApiImplicitParam(name = "id", value = "User database id", example = "1", required = true, dataType = "long", paramType = "path")
    })
    @GetMapping("/{id}")
    public ResponseEntity<User> findById(@PathVariable("id") Long userId) {

        return new ResponseEntity<>(userService.findOne(userId), HttpStatus.OK);
    }

    @ApiOperation("Search user by surname")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Successful loading user"),
            @ApiResponse(code = 500, message = "Server error, something wrong")
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "X-Auth-Token", value = "token", required = true, dataType = "string", paramType = "header"),
            @ApiImplicitParam(name = "query", value = "User database surname", example = "ketsko", required = true, dataType = "string", paramType = "query")
    })
    @JsonView(Views.FindWithoutRepairContracts.class)
    @GetMapping("/search")
    public List<User> searchUser(@RequestParam("query") String query) {

        return userService.searchBySurname(query);
    }

    @JsonView(Views.Create.class)
    @ApiOperation("Create user")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Successful, user created"),
            @ApiResponse(code = 422, message = "Failed user creation properties validation"),
            @ApiResponse(code = 500, message = "Server error, something wrong")
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "X-Auth-Token", value = "token", required = true, dataType = "string", paramType = "header"),
    })
    @PostMapping
    public ResponseEntity<User> create(@Valid @RequestBody UserCreateRequest userCreateRequest, @ApiIgnore Principal principal) {
        User convertedUser = conversionService.convert(userCreateRequest, User.class);

        String username = PrincipalUtil.getUsername(principal);

        log.info("User with login {} perform saving new entity", username);

        return new ResponseEntity<>(userService.save(convertedUser), HttpStatus.OK);
    }

    @ApiOperation("Update user")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Successful, user updated"),
            @ApiResponse(code = 404, message = "User was not found"),
            @ApiResponse(code = 422, message = "Failed user update properties validation"),
            @ApiResponse(code = 500, message = "Server error, something wrong")
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "X-Auth-Token", value = "token", required = true, dataType = "string", paramType = "header")
    })
    @JsonView(Views.Update.class)
    @PutMapping(path = "/{id}")
    public ResponseEntity<User> update(@PathVariable("id") Long userId, @Valid @RequestBody UserUpdateRequest userUpdateRequest) {
        userUpdateRequest.setId(userId);
        User user = conversionService.convert(userUpdateRequest, User.class);

        return new ResponseEntity<>(userService.save(user), HttpStatus.OK);
    }

    @ApiOperation(value = "Delete user by id")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Successful delete user"),
            @ApiResponse(code = 500, message = "Server error, something wrong")
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "X-Auth-Token", value = "token", required = true, dataType = "string", paramType = "header")
    })
    @DeleteMapping(path = "/{id}")
    public ResponseEntity<Long> delete(@PathVariable("id") Long userId) {
        userService.hardDelete(userId);
        return new ResponseEntity<>(userId, HttpStatus.OK);
    }
}
