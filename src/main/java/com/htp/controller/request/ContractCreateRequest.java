package com.htp.controller.request;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.validation.constraints.NotNull;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
@Builder
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
@ApiModel(description = "Contract create model")
public class ContractCreateRequest {

    @NotNull
    @ApiModelProperty(required = true, dataType = "date", notes = "contract date", example = "2020-07-27T16:21:17.016Z")
    private Date contractDate;

    @NotNull
    @ApiModelProperty(required = true, dataType = "date", notes = "contract issue date", example = "2020-07-27T16:21:17.016Z")
    private Date issueDate;

    @NotNull
    @ApiModelProperty(required = true, dataType = "date", notes = "contract return date", example = "2020-07-27T16:21:17.016Z")
    private Date returnDate;

    @NotNull
    @ApiModelProperty(required = true, dataType = "string", notes = "total rent price", example = "20.00")
    private Double rentPrice;

    @NotNull
    @ApiModelProperty(required = true, dataType = "long", notes = "user id", example = "1")
    private Long userId;

    @NotNull
    @ApiModelProperty(required = true, dataType = "List", notes = "tool's id", example = "[1, 2 , 99]")
    private List<Long> toolsId = Collections.emptyList();

}
