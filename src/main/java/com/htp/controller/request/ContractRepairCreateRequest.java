package com.htp.controller.request;

import io.swagger.annotations.ApiModel;
import lombok.*;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
@ApiModel(description = "Repair contract creation model")
public class ContractRepairCreateRequest {


    private Long serviceId;

    private Long toolId;

    private Double totalCostRepair;

}
