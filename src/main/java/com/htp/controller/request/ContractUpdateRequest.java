package com.htp.controller.request;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.validation.constraints.NotNull;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)

@ApiModel(description = "Contract update model")
public class ContractUpdateRequest extends ContractCreateRequest {
    @NotNull
    @ApiModelProperty(required = true, dataType = "long", notes = "Contract id", example = "1")
    private Long contractId;

    @ApiModelProperty(required = true, dataType = "boolean", notes = "finished contract status", example = "false")
    private boolean finished;
}
