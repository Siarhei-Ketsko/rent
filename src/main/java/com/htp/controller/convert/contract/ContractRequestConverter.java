package com.htp.controller.convert.contract;

import com.htp.controller.convert.EntityConverter;
import com.htp.controller.request.ContractCreateRequest;
import com.htp.domain.Contract;

public abstract class ContractRequestConverter<S, T> extends EntityConverter<S, T> {

    protected Contract doConvert(Contract contract, ContractCreateRequest contractCreateRequest) {

        contract.setContractDate(contractCreateRequest.getContractDate());
        contract.setIssueDate(contractCreateRequest.getIssueDate());
        contract.setReturnDate(contractCreateRequest.getReturnDate());

        return contract;
    }
}
