package com.htp.controller.convert.contract;

import com.htp.controller.request.ContractUpdateRequest;
import com.htp.domain.Contract;
import com.htp.domain.Tool;
import com.htp.domain.User;
import com.htp.exceptions.EntityNotFoundException;
import org.springframework.stereotype.Component;

import static java.util.Optional.ofNullable;

@Component
public class ContractChangeRequestConverter extends ContractRequestConverter<ContractUpdateRequest, Contract> {
    @Override
    public Contract convert(ContractUpdateRequest contractUpdateRequest) {
        Contract contract = ofNullable(entityManager.find(Contract.class, contractUpdateRequest.getContractId()))
                .orElseThrow(() -> new EntityNotFoundException(Contract.class, contractUpdateRequest.getContractId()));

        contractUpdateRequest.getToolsId().forEach(toolId -> contract.getTools().add(entityManager.find(Tool.class, toolId)));
        User user = entityManager.find(User.class ,contractUpdateRequest.getUserId());
        contract.setUser(user);
        contract.setRentPrice(contract.getTools().stream().mapToDouble(Tool::getPrice).sum());
        contract.setFinished(contractUpdateRequest.isFinished());
        contract.getTools().forEach(tool -> tool.getContracts().add(contract));
        user.getContracts().add(contract);
        if (contract.isFinished()) {
            contract.getTools().forEach(tool -> tool.setAvailability(true));
        }

        return doConvert(contract, contractUpdateRequest);
    }
}
