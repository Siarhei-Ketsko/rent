package com.htp.controller.convert.contract;

import com.htp.controller.request.ContractCreateRequest;
import com.htp.domain.Contract;
import com.htp.domain.Tool;
import com.htp.domain.User;
import org.springframework.stereotype.Component;

@Component
public class ContractCreateRequestConverter extends ContractRequestConverter<ContractCreateRequest, Contract> {

    @Override
    public Contract convert(ContractCreateRequest contractCreateRequest) {
        Contract contract = new Contract();
        contractCreateRequest.getToolsId().forEach(toolId -> contract.getTools().add(entityManager.find(Tool.class, toolId)));
        contract.setUser(entityManager.find(User.class ,contractCreateRequest.getUserId()));
        contract.setRentPrice(contract.getTools().stream().mapToDouble(Tool::getPrice).sum());
        contract.getTools().forEach(tool -> tool.getContracts().add(contract));
        entityManager.find(User.class, contractCreateRequest.getUserId()).getContracts().add(contract);

        return doConvert(contract, contractCreateRequest);
    }
}
