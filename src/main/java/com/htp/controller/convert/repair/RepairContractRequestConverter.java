package com.htp.controller.convert.repair;

import com.htp.controller.convert.EntityConverter;
import com.htp.controller.request.ContractRepairCreateRequest;
import com.htp.domain.ContractRepair;

public abstract class RepairContractRequestConverter<S, T> extends EntityConverter<S, T> {

    protected ContractRepair doConvert(ContractRepair contractRepair, ContractRepairCreateRequest contractRepairCreateRequest) {

        contractRepair.setTotalCostRepair(contractRepairCreateRequest.getTotalCostRepair());

        return contractRepair;
    }

}
