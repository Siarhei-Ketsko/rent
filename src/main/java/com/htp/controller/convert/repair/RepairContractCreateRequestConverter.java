package com.htp.controller.convert.repair;

import com.htp.controller.request.ContractRepairCreateRequest;
import com.htp.domain.ContractRepair;
import com.htp.domain.Service;
import com.htp.domain.Tool;
import org.springframework.stereotype.Component;

@Component
public class RepairContractCreateRequestConverter extends RepairContractRequestConverter<ContractRepairCreateRequest, ContractRepair> {

    @Override
    public ContractRepair convert(ContractRepairCreateRequest contractRepairCreateRequest) {
        ContractRepair contractRepair = new ContractRepair();
        contractRepair.setService(entityManager.find(Service.class, contractRepairCreateRequest.getServiceId()));
        contractRepair.setTool(entityManager.find(Tool.class, contractRepairCreateRequest.getToolId()));

        return doConvert(contractRepair, contractRepairCreateRequest);
    }
}
