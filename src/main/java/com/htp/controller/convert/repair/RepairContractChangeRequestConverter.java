package com.htp.controller.convert.repair;

import com.htp.controller.request.ContractRepairUpdateRequest;
import com.htp.domain.ContractRepair;
import com.htp.domain.Service;
import com.htp.domain.Tool;
import com.htp.exceptions.EntityNotFoundException;
import org.springframework.stereotype.Component;

import static java.util.Optional.ofNullable;

@Component
public class RepairContractChangeRequestConverter extends RepairContractRequestConverter<ContractRepairUpdateRequest, ContractRepair> {
    @Override
    public ContractRepair convert(ContractRepairUpdateRequest contractRepairUpdateRequest) {
        ContractRepair contractRepair = ofNullable(entityManager.find(ContractRepair.class, contractRepairUpdateRequest.getContractRepairId()))
                .orElseThrow(() -> new EntityNotFoundException(ContractRepair.class, contractRepairUpdateRequest.getContractRepairId()));
        contractRepair.setService(entityManager.find(Service.class, contractRepairUpdateRequest.getServiceId()));
        contractRepair.setTool(entityManager.find(Tool.class, contractRepairUpdateRequest.getToolId()));

        return doConvert(contractRepair, contractRepairUpdateRequest);
    }
}
