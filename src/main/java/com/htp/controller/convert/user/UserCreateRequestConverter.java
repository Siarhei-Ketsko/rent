package com.htp.controller.convert.user;

import com.htp.controller.request.UserCreateRequest;
import com.htp.domain.Role;
import com.htp.domain.Roles;
import com.htp.domain.User;
import org.springframework.stereotype.Component;

@Component
public class UserCreateRequestConverter extends UserRequestConverter<UserCreateRequest, User> {

    @Override
    public User convert(UserCreateRequest userCreateRequest) {
        User user = new User();

        Role role = new Role();
        role.setRoleName(Roles.ROLE_USER.name());
        role.setUser(user);
        user.setRole(role);

        return doConvert(user, userCreateRequest);

    }
}
