package com.htp.controller.convert.user;

import com.htp.controller.request.UserUpdateRequest;
import com.htp.domain.User;
import com.htp.exceptions.EntityNotFoundException;
import org.springframework.stereotype.Component;

import static java.util.Optional.ofNullable;


@Component
public class UserChangeRequestConverter extends UserRequestConverter<UserUpdateRequest, User> {

    @Override
    public User convert(UserUpdateRequest userUpdateRequest) {
        User user = ofNullable(entityManager.find(User.class, userUpdateRequest.getId()))
                .orElseThrow(() -> new EntityNotFoundException(User.class, userUpdateRequest.getId()));
        return doConvert(user, userUpdateRequest);
    }
}
