package com.htp.controller.convert.user;

import com.htp.controller.convert.EntityConverter;
import com.htp.controller.request.UserCreateRequest;
import com.htp.controller.request.UserUpdateRequest;
import com.htp.domain.User;

public abstract class UserRequestConverter<S, T> extends EntityConverter<S, T> {

    protected User doConvert(User user, UserCreateRequest userCreateRequest) {

        user.setUsername(userCreateRequest.getUsername());
        user.setSurname(userCreateRequest.getSurname());
        user.setPatronymic(userCreateRequest.getPatronymic());
        user.setPhone(userCreateRequest.getPhone());
        user.setSeriesPassport(userCreateRequest.getSeriesPassport());
        user.setNumberPassport(userCreateRequest.getNumberPassport());
        user.setAddress(userCreateRequest.getAddress());
        user.setLogin(userCreateRequest.getLogin());
        user.setPassword(userCreateRequest.getPassword());
        user.setEmail(userCreateRequest.getEmail());

        return user;
    }
    protected User doConvert(User user, UserUpdateRequest userUpdateRequest) {

        user.setUsername(userUpdateRequest.getUsername());
        user.setSurname(userUpdateRequest.getSurname());
        user.setPatronymic(userUpdateRequest.getPatronymic());
        user.setPhone(userUpdateRequest.getPhone());
        user.setSeriesPassport(userUpdateRequest.getSeriesPassport());
        user.setNumberPassport(userUpdateRequest.getNumberPassport());
        user.setAddress(userUpdateRequest.getAddress());
        user.setPassword(userUpdateRequest.getPassword());
        user.setEmail(userUpdateRequest.getEmail());
        user.setVerified(userUpdateRequest.isVerified());

        return user;
    }


}
