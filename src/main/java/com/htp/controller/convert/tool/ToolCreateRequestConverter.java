package com.htp.controller.convert.tool;

import com.htp.controller.request.ToolCreateRequest;
import com.htp.domain.Tool;
import org.springframework.stereotype.Component;

@Component
public class ToolCreateRequestConverter extends ToolRequestConverter<ToolCreateRequest, Tool> {

    @Override
    public Tool convert(ToolCreateRequest toolCreateRequest) {
        Tool tool = new Tool();

        return doConvert(tool, toolCreateRequest);
    }
}
