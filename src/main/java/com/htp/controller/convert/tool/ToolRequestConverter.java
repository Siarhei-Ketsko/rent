package com.htp.controller.convert.tool;

import com.htp.controller.convert.EntityConverter;
import com.htp.controller.request.ToolCreateRequest;
import com.htp.domain.Tool;

public abstract class ToolRequestConverter<S, T> extends EntityConverter <S, T> {

    protected Tool doConvert(Tool tool, ToolCreateRequest toolCreateRequest) {

        tool.setBrand(toolCreateRequest.getBrand());
        tool.setModel(toolCreateRequest.getModel());
        tool.setPersonalNumber(toolCreateRequest.getPersonalNumber());
        tool.setPrice(toolCreateRequest.getPrice());
        tool.setAvailability(toolCreateRequest.isAvailability());

        return tool;
    }
}
