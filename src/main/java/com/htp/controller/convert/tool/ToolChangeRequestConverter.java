package com.htp.controller.convert.tool;

import com.htp.controller.request.ToolUpdateRequest;
import com.htp.domain.Tool;
import com.htp.exceptions.EntityNotFoundException;
import org.springframework.stereotype.Component;

import static java.util.Optional.ofNullable;

@Component
public class ToolChangeRequestConverter extends ToolRequestConverter<ToolUpdateRequest, Tool> {
    @Override
    public Tool convert(ToolUpdateRequest toolUpdateRequest) {
        Tool tool = ofNullable(entityManager.find(Tool.class, toolUpdateRequest.getToolId()))
                .orElseThrow(() -> new EntityNotFoundException(Tool.class, toolUpdateRequest.getToolId()));
        return doConvert(tool, toolUpdateRequest);
    }
}
