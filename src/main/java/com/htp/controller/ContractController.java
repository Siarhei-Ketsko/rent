package com.htp.controller;

import com.htp.controller.request.ContractCreateRequest;
import com.htp.controller.request.ContractUpdateRequest;
import com.htp.domain.Contract;
import com.htp.service.ContractService;
import io.swagger.annotations.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.convert.ConversionService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/contracts")
public class ContractController {

   private ContractService contractService;

   private ConversionService conversionService;

    public ContractController(ContractService contractService, ConversionService conversionService) {
        this.contractService = contractService;
        this.conversionService = conversionService;
    }

    @ApiOperation(value = "Finding all contracts")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Successful loading contracts"),
            @ApiResponse(code = 500, message = "Server error, something wrong")
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "X-Auth-Token", value = "token", required = true, dataType = "string", paramType = "header")
    })
    @GetMapping
    public ResponseEntity<List<Contract>> findAll() {
        return new ResponseEntity<>(contractService.findAll(), HttpStatus.OK);
    }

    @ApiOperation(value = "Finding contract by id")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Successful loading contract"),
            @ApiResponse(code = 500, message = "Server error, something wrong")
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "Contract database id", example = "1", required = true, dataType = "long", paramType = "path"),
            @ApiImplicitParam(name = "X-Auth-Token", value = "token", required = true, dataType = "string", paramType = "header")
    })
    @GetMapping("/{id}")
    public Contract findById(@PathVariable("id") Long contractId) {
        return contractService.findOne(contractId);
    }

    @ApiOperation(value = "Create contract")
    @ApiResponses({
            @ApiResponse(code = 201, message = "Successful creation contract"),
            @ApiResponse(code = 422, message = "Failed contract creation properties validation"),
            @ApiResponse(code = 500, message = "Server error, something wrong")
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "X-Auth-Token", value = "token", required = true, dataType = "string", paramType = "header")
    })
    @PostMapping
    public ResponseEntity<Contract> create(@Valid @RequestBody ContractCreateRequest contractCreateRequest) {
        Contract convertedContract = conversionService.convert(contractCreateRequest, Contract.class);


        return new ResponseEntity<>(contractService.save(convertedContract), HttpStatus.OK);
    }

    @ApiOperation(value = "Update contract")
    @ApiResponses({
            @ApiResponse(code = 201, message = "Successful update contract"),
            @ApiResponse(code = 422, message = "Failed contract update properties validation"),
            @ApiResponse(code = 500, message = "Server error, something wrong")
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "contract database id", example = "1", required = true, dataType = "long", paramType = "path"),
            @ApiImplicitParam(name = "X-Auth-Token", value = "token", required = true, dataType = "string", paramType = "header")
    })
    @PutMapping(path="/{id}")
    public ResponseEntity<Contract>  update(@PathVariable("id") Long contractId, @Valid  @RequestBody ContractUpdateRequest contractUpdateRequest) {
        contractUpdateRequest.setContractId(contractId);
        Contract convertedContract = conversionService.convert(contractUpdateRequest, Contract.class);

        return new ResponseEntity<>(contractService.update(convertedContract), HttpStatus.OK);

    }

    @DeleteMapping(path = "/{id}/tools/{toolId}")
    public ResponseEntity<Long> deleteToolsFromContract(@PathVariable("id") Long contractId, @PathVariable ("toolId") Long toolId) {
        contractService.deleteToolsFromContract(contractId, toolId);
        return new ResponseEntity<>(contractId, HttpStatus.OK);
    }



}
