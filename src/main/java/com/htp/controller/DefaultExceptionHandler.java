package com.htp.controller;

import com.htp.controller.response.ErrorMessage;
import com.htp.exceptions.EntityNotFoundException;
import com.htp.exceptions.NotVerifyException;
import io.jsonwebtoken.JwtException;
import lombok.extern.slf4j.Slf4j;
import org.postgresql.util.PSQLException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.naming.AuthenticationException;

@Slf4j
@ControllerAdvice
public class DefaultExceptionHandler {

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ErrorMessage> handleMethodArgumentNotValidException(MethodArgumentNotValidException e) {
        log.error(e.getMessage(), e);
        return new ResponseEntity<>(new ErrorMessage(1L, e.getLocalizedMessage()), HttpStatus.UNPROCESSABLE_ENTITY);
    }

    @ExceptionHandler(AuthenticationException.class)
    public ResponseEntity<ErrorMessage> handleAuthenticationException(AuthenticationException e) {
        log.error(e.getLocalizedMessage(), e);
        return new ResponseEntity<>(new ErrorMessage(2L, e.getLocalizedMessage()), HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(EntityNotFoundException.class)
    public ResponseEntity<ErrorMessage> handleEntityNotFoundException(EntityNotFoundException e) {
        log.error(e.getLocalizedMessage(), e);
        return new ResponseEntity<>(new ErrorMessage(3L, e.getLocalizedMessage()), HttpStatus.NOT_FOUND);
    }
    @ExceptionHandler(JwtException.class)
    public ResponseEntity<ErrorMessage> handleMalformedJwtException(JwtException e) {
        log.error(e.getLocalizedMessage(), e);
        return new ResponseEntity<>(new ErrorMessage(4L, e.getLocalizedMessage()), HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(PSQLException.class)
    public ResponseEntity<ErrorMessage> handlePSQLException(PSQLException e) {
        log.error(e.getLocalizedMessage(), e);
        return new ResponseEntity<>(new ErrorMessage(5L, e.getLocalizedMessage()), HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(NotVerifyException.class)
    public ResponseEntity<ErrorMessage> handleVerifyError(NotVerifyException e) {
        log.error(e.getLocalizedMessage(), e);
        return new ResponseEntity<>(new ErrorMessage(6L, e.getLocalizedMessage()), HttpStatus.FORBIDDEN);
    }


    @ExceptionHandler(Exception.class)
    public ResponseEntity<ErrorMessage> handleOthersException(Exception e) {
        /* Handles all other exceptions. Status code 500. */
        log.error(e.getMessage(), e);
        log.info(e.getMessage(), e);
        return new ResponseEntity<>(new ErrorMessage(e.getMessage()),
                HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
