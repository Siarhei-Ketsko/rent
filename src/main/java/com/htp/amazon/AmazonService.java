package com.htp.amazon;

import org.springframework.web.multipart.MultipartFile;

public interface AmazonService {

    String uploadFile(MultipartFile multipartFile, Long idEntity, String folderNameEntity);

    String deleteFileFromS3Bucket(String imageLink, Long idEntity, String folderNameEntity);

}
