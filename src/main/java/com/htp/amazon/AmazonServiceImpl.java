package com.htp.amazon;

import com.htp.config.AmazonConfiguration;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import software.amazon.awssdk.core.sync.RequestBody;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.DeleteObjectRequest;
import software.amazon.awssdk.services.s3.model.PutObjectRequest;
import software.amazon.awssdk.services.s3.model.PutObjectResponse;

import java.io.IOException;
import java.util.UUID;

@Slf4j
@Service
public class AmazonServiceImpl implements AmazonService {

    private S3Client s3Client;

    private AmazonConfiguration configuration;

    public static final String AMAZON_IMAGE_LINK_TEMPLATE = "%s/%s/%s/%s/%s.jpg";

    public AmazonServiceImpl(S3Client s3Client, AmazonConfiguration configuration) {
        this.s3Client = s3Client;
        this.configuration = configuration;
    }

    @Override
    public String uploadFile(MultipartFile multipartFile, Long idEntity, String folderNameEntity) {

        byte[] image = convertMultiPartToByte(multipartFile);

        return uploadFileTos3bucket(image, idEntity, folderNameEntity);

    }

    @Override
    public String deleteFileFromS3Bucket(String imageLink, Long idEntity, String folderNameEntity) {
        String imageUUID = imageLink.substring(imageLink.lastIndexOf("/") + 1);
        log.info(imageUUID);
        s3Client.deleteObject(DeleteObjectRequest.builder()
                .bucket(configuration.getBucketName())
                .key(String.format("%s/%s/%s", folderNameEntity, idEntity, imageUUID))
                .build()
        );
        return "image";
    }

    private String uploadFileTos3bucket(byte[] image, Long idEntity, String folderNameEntity) {

        String imageUUID = UUID.randomUUID().toString();

        PutObjectResponse putObjectResponse = s3Client.putObject(PutObjectRequest.builder()
                .bucket(configuration.getBucketName())
                .contentType("image/jpeg")
                .contentLength((long) image.length)
                .acl("public-read")
                .key(String.format("%s/%s/%s.jpg", folderNameEntity, idEntity, imageUUID))
                .build(), RequestBody.fromBytes(image));

        log.info(putObjectResponse.ssekmsKeyId());
        return generateImageLink(idEntity, imageUUID, folderNameEntity);
    }

    private byte[] convertMultiPartToByte(MultipartFile multipartFile) {
        try {
            return multipartFile.getBytes();

        } catch (IOException e) {
            throw new RuntimeException("Image uploading error!");
        }
    }

    private String generateImageLink(Long idEntity, String imageUUID, String folderNameEntity) {

        return String.format(
                AMAZON_IMAGE_LINK_TEMPLATE,
                configuration.getEndpointUrl(),
                configuration.getBucketName(),
                folderNameEntity,
                idEntity,
                imageUUID
        );
    }
}
